#include "DBJobs.h"

#include <QThreadStorage>

QThreadStorage<QMap<QString, QSqlDatabase>* > g_caches;

void CacheObject(QString const& p_key, QSqlDatabase p_object)
{
    if (not g_caches.hasLocalData())
        g_caches.setLocalData(new QMap<QString, QSqlDatabase>);

    g_caches.localData()->insert(p_key, p_object);
}

void RemoveFromCache(QString const& key)
{
    if (!g_caches.hasLocalData())
        return;

    g_caches.localData()->remove(key);
}

QSqlDatabase GetFromCache(QString const& key)
{
    if (!g_caches.hasLocalData())
        return QSqlDatabase();

    return (*g_caches.localData())[key];
}

QString CreateDbConnectionName(QString const&  p_dbName)
{
    qDebug() << "thread name: " << QThread::currentThreadId();

    // todo: QThread::currentThreadId() should rather not be used. What instead?
    return p_dbName + QString::number((quint64) QThread::currentThreadId());
}

QSqlDatabase CreateDbConnection(QString const&  p_conName, QString const& p_dbName)
{
    qDebug() << " creating connection " << p_conName << " to " << p_dbName;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", p_conName);  // hardcoded SQL driver
    db.setDatabaseName(p_dbName);
    //db.setConnectOptions("QSQLITE_BUSY_TIMEOUT=2"); // any value given makes 0 timeout
    if (not db.open())
    {
        qWarning() << "db could not be opened: " << db.lastError() << ", con. name: "
                 << db.connectionName() << ", db name: " << db.databaseName();
    }
    return db;
}

QSqlDatabase GetDbConnection(QString const& p_dbName)
{
    QSqlDatabase db = GetFromCache(p_dbName);

    if (not db.isValid())
    {
        QString conName = CreateDbConnectionName(p_dbName);
        db = CreateDbConnection(conName, p_dbName);
        CacheObject(p_dbName, db);
    }

    return db;
}

BasicQuery::BasicQuery(QString const& p_query, QString const& p_dbName,
          DBHandler::basic_callback p_callback)
    : m_query(p_query),
      m_dbName(p_dbName),
      m_callback(p_callback)
{
    setAutoDelete(true);
}


void BasicQuery::run()
{
    qDebug() << "___starting BasicQuery: " << m_query;

    QSqlDatabase db = GetDbConnection(m_dbName);
    QSqlQuery q(m_query, db);

    qDebug() << "___finishing BasicQuery: " << m_query;

    emit queryFinished(q.lastError(), m_callback);
}



SelectQuery::SelectQuery(QString const& p_query, QString const& p_dbName,
            DBHandler::select_callback p_callback,
            QList<QVariant> const& p_paramsList)
    : m_query(p_query),
      m_dbName(p_dbName),
      m_callback(p_callback),
      m_paramsList(p_paramsList)
{
    setAutoDelete(true);
}

void SelectQuery::run()
{
    qDebug() << "___starting SelectQuery: " << m_query;

    QSqlError error;
    QList<QSqlRecord> results;
    QSqlDatabase db = GetDbConnection(m_dbName);

    QSqlQuery q(db);
    if (q.prepare(m_query))
    {
        for(auto& param : m_paramsList) { q.addBindValue(param); }
        q.exec();

        error = q.lastError();
        if (not error.isValid())
        {
            while (q.next())
                results << q.record();
        }
    }
    else
        error = q.lastError();

    qDebug() << "___finishing SelectQuery: " << m_query;

    emit queryFinished(error, results, m_callback);
}


CloseConnection::CloseConnection(const QString &p_dbName) : m_dbName(p_dbName)
{
    setAutoDelete(true);
}

void CloseConnection::run()
{
    QString dbCon2Remove;
    {
        QSqlDatabase db = GetFromCache(m_dbName);
        if (db.isValid())
        {
            qDebug() << "closing connection ";
            dbCon2Remove = db.connectionName();
            db.close();
            //qDebug() << "closed connection ";
            RemoveFromCache(m_dbName);
            qDebug() << "removed from cache ";
        }
        else
            qDebug() << "there is no valid connection to be removed for " << m_dbName;
    }

    if (dbCon2Remove.size())
    {
        qDebug() << "removing database " << dbCon2Remove;
        QSqlDatabase::removeDatabase(dbCon2Remove);
        qDebug() << "database removed";
    }
}
