#include "DBHandler.h"
#include "DBManager.h"

// FACTORY

DBHandlerFactory::DBHandlerFactory(QObject *p_parent) : QObject(p_parent)
{
    m_manager = new DBManager(this);
}

DBHandlerFactory::~DBHandlerFactory()
{
}

std::unique_ptr<DBHandler> DBHandlerFactory::CreateDBHandler(const QString &p_dbName)
{
    DBHandler* handler= new DBHandler(p_dbName);

    connect(handler, SIGNAL(queryRequested(QString,QString,DBHandler::basic_callback,const QObject*,const char*)),
            m_manager, SLOT(runQuery(QString const&, QString const&, DBHandler::basic_callback,
                                     const QObject*, const char *)));
    connect(handler, SIGNAL(selectQueryRequested(QString,QString,QList<QVariant>,DBHandler::select_callback,const QObject*,const char*)),
            m_manager, SLOT(runQuery(const QString &, QString const&, QList<QVariant> const&, DBHandler::select_callback,
                                     const QObject*, const char*)));
    connect(handler, SIGNAL(destroyed(QString,DBHandler*)),
            m_manager, SLOT(unregisterHandler(QString,DBHandler*)));
    m_manager->registerHandler(p_dbName, handler);

    return std::unique_ptr<DBHandler>(handler);
}


// HANDLER

DBHandler::DBHandler(QString const& p_dbName) : m_dbName(p_dbName)
{
}

DBHandler::~DBHandler()
{
    emit destroyed(m_dbName, this);
}


void DBHandler::query(QString const& p_query, basic_callback p_callback)
{
    emit queryRequested( p_query, m_dbName, p_callback,
                         this,
                         SLOT(onQueryFinished(QSqlError, DBHandler::basic_callback)) );
}


void DBHandler::onQueryFinished(QSqlError p_error, basic_callback p_callback)
{
    qDebug() << "error code of query is " << p_error;
    p_callback(p_error);
}

void DBHandler::onSelectQueryFinished(QSqlError p_error, QList<QSqlRecord> const& p_results, select_callback p_callback)
{
    qDebug() << "error code of select query is " << p_error;
    p_callback(p_error, p_results);
}

