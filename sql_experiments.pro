#-------------------------------------------------
#
# Project created by QtCreator 2013-10-31T18:22:49
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

QMAKE_CXXFLAGS += -std=c++0x

TARGET = sql_experiments
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    DBHandler.cpp \
    DBJobs.cpp \
    DBManager.cpp

HEADERS += \
    DBHandler.h \
    DBManager.h \
    DBJobs.h
