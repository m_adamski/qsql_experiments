#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QObject>
#include <QThreadPool>

#include "DBHandler.h"

class DBManager : public QObject
{
    Q_OBJECT
public:
    DBManager(QObject* p_parent);

public slots:
    void registerHandler(QString const& p_dbName, DBHandler* p_handler);
    void unregisterHandler(QString const& p_dbName, DBHandler* p_handler);

    void runQuery(QString const& p_query, QString const& p_dbName, DBHandler::basic_callback p_callback,
                  const QObject* p_dbHandler, const char *p_slot);
    void runQuery(QString const& p_query, QString const& p_dbName, QList<QVariant> const& p_paramsList, DBHandler::select_callback p_callback,
                  const QObject* p_dbHandler, const char* p_slot);

private:
    QThreadPool m_threads;
    QMultiMap<QString, DBHandler*> m_dbHandlers;
};

#endif // DBMANAGER_H
