#ifndef DBHANDLER_H
#define DBHANDLER_H

#include <QtSql/QtSql>
#include <QList>

#include <memory>
#include <functional>

class DBManager;
class DBHandler;

class DBHandlerFactory : public QObject
{
public:
    DBHandlerFactory(QObject* p_parent = NULL);
    ~DBHandlerFactory();

    std::unique_ptr<DBHandler> CreateDBHandler(QString const& p_dbName);
private:
    DBManager* m_manager;
};

class DBHandler : public QObject
{
    friend class DBManager;
    Q_OBJECT
public:
    /** @param p_dbName Path to database. If database file does not exist, it will be created. */
    DBHandler(QString const& p_dbName);
    virtual ~DBHandler();

    typedef std::function<void (QSqlError const&)> basic_callback;
    typedef std::function<void (QSqlError const&, QList<QSqlRecord> const&)> select_callback;

    void query(QString const& p_query, basic_callback);

    template <typename... Args_t>
    void selectQuery(QString const& p_query, select_callback p_callback, Args_t&&... p_args)
    {
        QList<QVariant> paramsList;
        fillParams(paramsList, std::forward<Args_t>(p_args)...);

        emit selectQueryRequested(p_query, m_dbName, paramsList, p_callback,
                                  this,
                                  SLOT(onSelectQueryFinished(QSqlError, QList<QSqlRecord> const&,
                                                             DBHandler::select_callback)));
    }

signals:

    void queryRequested(QString const& p_query, QString const& p_dbName,
                        DBHandler::basic_callback p_callback,
                        const QObject *p_dbHandler, const char *p_slot);
    void selectQueryRequested(QString const& p_query, QString const& p_dbName,
                        QList<QVariant> const& p_paramsList,
                        DBHandler::select_callback p_callback,
                        const QObject* p_dbHandler, const char* p_slot);
    void destroyed(QString const& p_dbName, DBHandler* handler);

private slots:
    void onQueryFinished(QSqlError, DBHandler::basic_callback);
    void onSelectQueryFinished(QSqlError, QList<QSqlRecord> const&, DBHandler::select_callback);

private:
    template <typename... Args_t>
    static void fillParams(QList<QVariant>& p_paramsList, const QVariant&& p_param, Args_t&&... p_args)
    {
        p_paramsList << p_param;
        fillParams(p_paramsList, std::forward<Args_t>(p_args)...);
    }

    static void fillParams(QList<QVariant>& /*p_paramsList*/)
    {
    }

    QString m_dbName;
};

#endif // DBHANDLER_H
