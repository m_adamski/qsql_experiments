#ifndef DBJOBS_H
#define DBJOBS_H

#include "DBHandler.h"  // it would be nice to remove this dependency on:
                        // DBHandler::basic_callback and DBHandler::select_callback

#include <QObject>
#include <QRunnable>

class BasicQuery : public QObject, public QRunnable
{
    Q_OBJECT
public:
    BasicQuery(QString const& p_query, QString const& p_dbName,
              DBHandler::basic_callback p_callback);
    virtual ~BasicQuery() {}

    virtual void run();

signals:
    void queryFinished(QSqlError, DBHandler::basic_callback);

private:
    const QString m_query;
    const QString m_dbName;
    const DBHandler::basic_callback m_callback;
    const QList<QVariant> m_paramsList;
};

class SelectQuery : public QObject, public QRunnable
{
    Q_OBJECT
public:
    SelectQuery(QString const& p_query, QString const& p_dbName,
                DBHandler::select_callback p_callback,
                QList<QVariant> const& p_paramsList);
    virtual ~SelectQuery() {}

    virtual void run();

signals:
    void queryFinished(QSqlError, QList<QSqlRecord> const&, DBHandler::select_callback);

private:
    const QString m_query;
    const QString m_dbName;
    const DBHandler::select_callback m_callback;
    const QList<QVariant> m_paramsList;
};

class CloseConnection : public QRunnable
{
public:
    CloseConnection(QString const& p_dbName);
    virtual ~CloseConnection() {}

    virtual void run();

private:
    const QString m_dbName;
};

#endif // DBJOBS_H
