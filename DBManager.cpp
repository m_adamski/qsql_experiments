#include "DBManager.h"

#include "DBJobs.h"

#include <QMetaType>

DBManager::DBManager(QObject* p_parent) : QObject(p_parent)
{
    /** @note If you want to change number of threads, make sure that all threads will close
     * their db connections. Currently it is guaranteed for only 1 thread in pool */
    const int MAX_THREADS = 1;
    m_threads.setMaxThreadCount(MAX_THREADS);

    qRegisterMetaType<QSqlError>("QSqlError");
    qRegisterMetaType<QList<QSqlRecord>>("QList<QSqlRecord>");
    qRegisterMetaType<DBHandler::basic_callback>("DBHandler::basic_callback");
    qRegisterMetaType<DBHandler::select_callback>("DBHandler::select_callback");
}

void DBManager::registerHandler(QString const& p_dbName, DBHandler *p_handler)
{
    qDebug() << "register " << p_dbName << ", handler: " << p_handler;
    m_dbHandlers.insert(p_dbName, p_handler);
    qDebug() << "map has " << m_dbHandlers.values(p_dbName).count() << " values for key " << p_dbName;
}

void DBManager::unregisterHandler(QString const& p_dbName, DBHandler *p_handler)
{
    qDebug() << "unregister " << p_dbName << ", handler: " << p_handler;
    m_dbHandlers.remove(p_dbName, p_handler);
    if (not m_dbHandlers.contains(p_dbName))
    {
        qDebug() << "requesting to close connection to " << p_dbName;
        CloseConnection* job = new CloseConnection(p_dbName);
        m_threads.start(job);
    }
}

void DBManager::runQuery(QString const& p_query, QString const& p_dbName, DBHandler::basic_callback p_callback,
              const QObject *p_dbHandler, const char *p_slot)
{
    BasicQuery* dbJob = new BasicQuery(p_query, p_dbName, p_callback);
    QObject::connect(dbJob, SIGNAL(queryFinished(QSqlError, DBHandler::basic_callback)),
                     p_dbHandler, p_slot);
    m_threads.start(dbJob);
}

void DBManager::runQuery(QString const& p_query, QString const& p_dbName, QList<QVariant> const& p_paramsList, DBHandler::select_callback p_callback,
              const QObject* p_dbHandler, const char* p_slot)
{
    SelectQuery* dbJob = new SelectQuery(p_query, p_dbName, p_callback, p_paramsList);
    QObject::connect(dbJob, SIGNAL(queryFinished(QSqlError, QList<QSqlRecord> const&, DBHandler::select_callback)),
                     p_dbHandler, p_slot);
    m_threads.start(dbJob);
}
